<?php 

namespace Afip\Ws;

use Exception;
use Afip\Ws\Tickets\Acceso\Tra;
use Afip\Ws\Tickets\Autenticacion\Ta;

/**
 * Factory Class para los Webservice de negocio de AFIP
 *
 * @package Afip\Ws
 */
class Wsn
{
	/**
	 * @param Config $config
	 * @return Wsaa
	 */
	public static function crearAutorizacion(Config $config)
	{
		//Ticket de Autorizacion
		$ta  = new Ta($config);
		//Requerimiento de Acceso
		$tra = new Tra($config);

		$oWsaa = new Wsaa($config, $ta, $tra);

		return $oWsaa;
	}

	/**
	 * Crea la instancia del webservice para Facturacion Electronica
	 *
	 * @param string $webserviceName
	 * @param Config $config
	 * @return null|object
	 */
	public static function crear($webserviceName, Config $config)
	{
		$oWs = null;
		$sWsClassname = null;

		switch ($webserviceName) {
			//Ws Fcturacion Electronica
			case (Wsfe::WS_NAME):
				$sWsClassname = Wsfe::class;
				break;
		}

		if (!empty($sWsClassname)) {
			$oAutorizacion = self::crearAutorizacion($config);
			$oWs = new $sWsClassname($oAutorizacion, $config);
		}

		return $oWs;
	}
}