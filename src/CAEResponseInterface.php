<?php 

namespace Afip\Ws;

interface CAEResponseInterface
{
	public function setCaeResponse ($feCaeReq, $feCabResp, $feDetResp);
}