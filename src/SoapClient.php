<?php

namespace Afip\Ws;

use SplSubject;
use SplObjectStorage;

class SoapClient extends \SoapClient implements \SplSubject
{
    /**
     * @var \SplObjectStorage
     */
    public $observers;

    /**
     * @var string
     */
    protected $lastResponse;

    /**
     * @param string $wsdl
     */
    public function __construct($wsdl, $endpoint)
    {
        $this->observers = new SplObjectStorage();

        parent::__construct($wsdl, [
            'soap_version'   => SOAP_1_2,
            'location'       => $endpoint,
            'trace'          => true,
            'exceptions'     => true
        ]);
    }

    /**
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param int $one_way
     * @return string
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $this->lastResponse = $response = parent::__doRequest($request, $location, $action, $version, $one_way);

        if ($this->observers->count() > 0) {
            $this->notify();
        }

        return $response;
    }

    /**
     * @param \SplObserver $observer
     */
    public function attach(\SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    /**
     * @param \SplObserver $observer
     */
    public function detach(\SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     *
     */
    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }

    }
}