<?php

namespace Afip\Ws;

class FeCAEResponse
{
    private $respuesta;

    protected $cabecera = [];

    protected $detalle = [];

    protected $observaciones = [];

    protected $eventos = [];

    protected $errores = [];

    const ESTADO_RECHAZADO = 'R';

    const ESTADO_PARCIAL = 'P';

    const ESTADO_APROBADO = 'A';

    public function __construct(\stdClass $response)
    {
        $this->respuesta = $response;
    }

    private static function parseObjectToArray(\stdClass $object)
    {
        $aReturn = [];
        $child = current($object);

        if (is_object($child)) {
            $aReturn[] = (array) $child;
        } else if(is_array($child)) {
            $aChild = json_decode(json_encode($child), true);
            $aReturn = array_merge($aReturn, $aChild);
        }

        return $aReturn;
    }

    public function isFiscalizada()
    {
        return ($this->respuesta->FeCabResp->Resultado === self::ESTADO_APROBADO);
    }

    public function hasObservaciones()
    {
        return !empty($this->respuesta->FeDetResp->FECAEDetResponse->Observaciones);
    }

    public function getObservaciones ()
    {
        if ($this->hasObservaciones()) {
            $observaciones = $this->respuesta->FeDetResp->FECAEDetResponse->Observaciones;
            $this->observaciones = self::parseObjectToArray($observaciones);
        }

        return $this->observaciones;
    }

    public function getErrors()
    {
        if (empty($this->errores)) {
            $errores = $this->respuesta->Errors;
            $this->errores = self::parseObjectToArray($errores);
        }

        return $this->errores;
    }

    public function hasErrors()
    {
        return !empty($this->respuesta->Errors);
    }

    public function getEventos()
    {
        if ($this->hasEventos()) {
            $eventos = $this->respuesta->Events;
            $this->eventos = self::parseObjectToArray($eventos);
        }

        return $this->eventos;
    }

    public function hasEventos()
    {
        return !empty($this->respuesta->Events);
    }
}