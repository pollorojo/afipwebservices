<?php 

namespace Afip\Ws;

use Exception;
use Afip\Ws\SoapClient as AfipClient;

class Wsfe extends AfipClient
{
	private $errors = [];
	
	private $wsaa;

	const WS_NAME = "wsfe";

	const WSDL_TESTING = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx";

	const WSDL_PRODUCTION = "https://servicios1.afip.gov.ar/wsfev1/service.asmx";

	/**
	 * @param Wsaa $oWsaa
	 * @param array $config
	 */
	public function __construct(Wsaa $oWsaa, $config)
	{
		$this->wsaa = $oWsaa;

		$wsdl 	  = Config::getWsdl(Wsfe::WS_NAME);
		$endpoint = ($config->isEnv(Config::ENV_TESTING)) ? Wsfe::WSDL_TESTING : Wsfe::WSDL_PRODUCTION;
		parent::__construct($wsdl, $endpoint);
	}

	/**
	 * Parsea los errores recibidos del webservice
	 *
	 * @param object $response
	 * @return boolean
	 */
	private function parseErrors ($response, $method)
	{
		$bReturn = false;

		if (isset($response->Errors) and $method != 'FEDummy') {	

			$errors = json_decode(json_encode($this->toArray($response->Errors->Err)));
			$this->errors = array_merge($this->errors, $errors);

			$bReturn =  true;
		}

		return $bReturn;
	}

	public function hasErrors ()
	{
		return !empty($this->errors);
	}

	public function getErrors ()
	{
		return $this->errors;
	}

	public function getWsaa()
	{
		return $this->wsaa;
	}

	/**
	 * Devuelve siempre un array
	 *
	 * @param mixed $content
	 * @return array
	 */
	private function toArray($content)
	{
		return (is_array($content)) ? $content : array((array) $content);
	}


	/**
	 * Recibe la información de un comprobante o lote de comprobantes.
	 *
	 * @param array $FeCAEReq
	 * @return object
	 */
	protected function FECAESolicitar($FeCAEReq, CAEResponseInterface $element = null)
	{
		return [
			'Auth'     => $this->wsaa->getAuth(),
			'FeCAEReq' => $FeCAEReq['FeCAEReq']
		];
	}


	/**
	 * Método para consultar Comprobantes Emitidos y su código
	 *
	 * @param integer $CbteTipo
	 * @param integer $CbteNro
	 * @param integer $PtoVta
	 * @return array
	 */
	protected function FECompConsultar($CbteTipo, $CbteNro, $PtoVta)
	{
		return [
			'Auth'          => $this->wsaa->getAuth(),
			'FeCompConsReq' => [
				'CbteTipo'  => $CbteTipo,
				'CbteNro'   => $CbteNro,
				'PtoVta'    => $PtoVta
			]
		];
	}    


	/**
	 * Retorna la cantidad máxima de registros que se podrá incluir en un request al método
	 * FECAESolicitar / FECAEARegInformativo
	 *
	 * @return array
	 */
	protected function FECompTotXRequest()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Este método retorna el universo de tipos de documentos disponibles en el presente WS.
	 *
	 * @return array
	 */
	protected function FEParamGetTiposDoc()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Retorna el ultimo comprobante autorizado para el tipo de 
	 * comprobante / cuit / punto de venta ingresado / Tipo de Emisión
	 * 
	 * @param integer $PtoVta
	 * @param integer $CbteTipo
	 * @return array
	 */
	protected function FECompUltimoAutorizado($PtoVta, $CbteTipo)
	{
		return [
			'Auth'     => $this->wsaa->getAuth(),
			'PtoVta'   => $PtoVta,
			'CbteTipo' => $CbteTipo
		];
	}

	/**
	 * Este método permite consultar los tipos de comprobantes habilitados en este WS
	 *
	 * @return array
	 */
	protected function FEParamGetTiposCbte()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Este método permite consultar los puntos de venta para ambos tipos de Código de Autorización
	 * (CAE y CAEA) gestionados previamente por la CUIT emisora.
	 *
	 * @return array
	 */
	protected function FEParamGetPtosVenta()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Este método devuelve los tipos de conceptos posibles en este WS.
	 *
	 * @return array
	 */
	protected function FEParamGetTiposConcepto()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Este método permite consultar los códigos y descripciones de los tipos de datos Opcionales que se
	 * encuentran habilitados para ser usados en el WS
	 *
	 * @return array
	 */
	protected function FEParamGetTiposOpcional()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Mediante este método se obtiene la totalidad de alícuotas de IVA posibles de uso en el presente
	 * WS, detallando código y descripción.
	 *
	 * @return array
	 */
	protected function FEParamGetTiposIva()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Devuelve los posibles códigos de tributos que puede contener un comprobante y su descripción.
	 *
	 * @return array
	 */
	protected function FEParamGetTiposTributos()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Este método retorna el universo de Monedas disponibles en el presente WS, indicando id y
	 * descripción de cada una.
	 * 
	 * @return array
	 */
	protected function FEParamGetTiposMonedas()
	{
		return ['Auth' => $this->wsaa->getAuth()];
	}


	/**
	 * Retorna la última cotización de la base de datos aduanera de la moneda ingresada. Este valor es
	 * orientativo.
	 *
	 * @param string $MonId
	 * @return array
	 */
	protected function FEParamGetCotizacion($MonId)
	{
		return ['Auth' => $this->wsaa->getAuth(), 'MonId' => $MonId];
	}


	/**
	 * Método Dummy para verificación de funcionamiento de infraestructura
	 *
	 * @return 
	 */
	protected function FEDummy()
	{
		return [];
	}

	/**
	 * @param string $methodName
	 * @param string $params
	 * @return mixed|null
	 * @throws Exception
	 */
	public function __call ($methodName, $params)
	{
		$response = null;

		if (!$this->wsaa->isLogued()) {
			$this->wsaa->login(self::WS_NAME);
		}

		if (method_exists($this, $methodName)) {
			$methodValues = call_user_func_array([$this, $methodName], $params);
		} else {
			$methodValues = $params;
		}

		try {
			$result = parent::__soapCall($methodName, [$methodValues]);

			if (is_soap_fault($result)) {
				throw new Exception("SoapFault::".$result->faultcode.": ".$result->faultstring.". [".$methodName."]", 1);
			}

			$response = $result->{$methodName."Result"};

			$this->parseErrors($response, $methodName);

			return $response;
		} catch (SoapFault $e) {
			die($e->getMessage());
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), 1, $e->getPrevious());
		}

		return $response;
	}

}