<?php 

namespace Afip\Ws;

use Exception;
use Afip\Ws\SoapClient as AfipClient;
use Afip\Ws\Tickets\Acceso\Tra;
use Afip\Ws\Tickets\Autenticacion\Ta;

/**
 * Class Wsaa
 * @package Afip\Ws
 */
class Wsaa extends AfipClient
{
	private $ta;

	private $tra;

	protected $ticketAuthContent;

	protected $webserviceName;

	const WS_NAME = 'wsaa';

	const WSDL_TESTING = 'https://wsaahomo.afip.gov.ar/ws/services/LoginCms';

	const WSDL_PRODUCTION = 'https://wsaa.afip.gov.ar/ws/services/LoginCms';

	public function __construct(Config $config, Ta $ta, Tra $tra)
	{
		$this->config = $config;
		$this->ta     = $ta;
		$this->tra    = $tra;

		$wsdl 	  = Config::getWsdl(self::WS_NAME);
		$endpoint = ($this->config->isEnv(Config::ENV_TESTING)) ? self::WSDL_TESTING : self::WSDL_PRODUCTION;

		parent::__construct($wsdl, $endpoint);
	}

	/**
	 * @return bool
	 */
	public function isLogued()
	{
		return $this->ta->isValid();
	}

	/**
	 * @param string $webserviceName
	 * @return array|null
	 * @throws Exception
	 */
	public function login ($webserviceName)
	{
		$return = null;

		//Si la autorizacion venció, loguear nuevamente
		if (!$this->isLogued()) {
			try {
				//TRA
				$cms = $this->crearFirmaAutorizacion($webserviceName);

				// Llamar al WebService de Autenticación y Autorización para obtener
				// el acceso a los WebServices de Negocio (WSN) ofrecidos por la AFIP
				$results = $this->loginCms(['in0' => $cms]);

				if (is_soap_fault($results)) {
					throw new Exception(__METHOD__.": Fallo en conexion SOAP: ".$results->faultstring." [".$results->faultcode."]", 1);
				}

				//TA
				if (!$this->ta->crearArchivo( $results->loginCmsReturn )) {
					throw new Exception(__METHOD__.": No se pudo crear el archivo de Ticket de Acceso (".$this->ta->getRealPath().")", 1);
				}

				$this->ticketAuthContent = $this->ta->read();

			} catch (Exception $e) {
				throw new Exception($e->getMessage(), 1, $e);
			}
		}

		return $this->getAuth();
	}

	/**
	 * @param string $webserviceName
	 * @return mixed
	 * @throws Exception
	 */
	private function crearFirmaAutorizacion ($webserviceName)
	{
		//Generar el mensaje del TRA (LoginTicketRequest.xml) 
		$this->tra->create(["service" => $webserviceName]);

		//Generar un CMS que contenga el TRA, su firma electrónica y el certificado X.509 (LoginTicketRequest.xml.cms) 
		//Codificar en Base64 el CMS (LoginTicketRequest.xml.cms.bse64) 
		return $this->tra->sign($this->config->crt, $this->config->key);		
	}

	/**
	 * @return array|null
	 * @throws Exception
	 */
	public function getAuth ()
	{
		$ta = (empty($this->ticketAuthContent)) ? $this->ta->read() : $this->ticketAuthContent;

		$aAuth = null;

		if (!empty($ta)) {
			$aAuth = [
				"Token" => $ta['token'],
				"Sign"  => $ta['sign'],
				"Cuit"  => $this->config->cuit
			];
		} else {
			throw new Exception("No se pudo leer los datos de autenticacion.", 1);
		}

		return $aAuth;
	}
}