<?php

namespace Afip\Ws\Tickets\Acceso;

use Afip\Ws\Config;
use SimpleXMLElement;
use Exception;

/**
 * Class Tra
 * @package Afip\Ws\Tickets\Acceso
 */
class Tra
{
	/**
	 * @var string
	 */
	private $xmlFilePath;

	/**
	 * @var string
	 */
	private $tmpFilePath;

	/**
	 * @var Config
	 */
	private $oConfig;

	/**
	 * @param Config $config
	 */
	public function __construct(Config $config)
	{
		$this->oConfig = $config;

		$this->xmlFilePath = $config->getStoragePath() . DIRECTORY_SEPARATOR . $config->alias . "_tra.xml";
		$this->tmpFilePath = $config->getStoragePath() . DIRECTORY_SEPARATOR . $config->alias . "_tra.tmp";
	}

	/**
	 * Crea el ticket TRA XML
	 *
	 * @return SimpleXMLElement
	 */
	public function crearArchivoXML(array $contenido = [])
	{
		if (!is_file($this->xmlFilePath)) {
			@fopen($this->xmlFilePath, "x+");
		}

		$xml = new SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>'.
			'<loginTicketRequest version="1.0"></loginTicketRequest>'
		);

		$xml->addChild('header');
		$xml->header->addChild('uniqueId',date('U'));
		$xml->header->addChild('generationTime',date('c',date('U')-600));
		$xml->header->addChild('expirationTime',date('c',date('U')+600));

		//Contenido personalizado
		$limite = count($contenido);

		if ($limite > 0) {
			foreach ($contenido as $key => $value) {
				//Clave debe ser string
				if (is_string($key)) {
					$xml->addChild($key, $value);
				}
			}
		}

		return $xml->asXML($this->xmlFilePath);
	}

	/**
	 * Crea un nuevo ticket TRA XML
	 *
	 * @return void
	 */
	public function create ($params)
	{
		$this->crearArchivoXML($params);
	}

	/**
	 * @return string
	 */
	public function obtenerContenidoArchivoTmp()
	{
		$inf = fopen($this->tmpFilePath, "r");
		$i   = 0;
		$cms = "";

		while (!feof($inf)) {
			$buffer = fgets($inf);

			if ( $i++ >= 4 ) {
				$cms.=$buffer;
			}
		}

		fclose($inf);

		return $cms;
	}

	/**
	 * @param string $cert
	 * @param string $privateKey
	 * @return string
	 * @throws Exception
	 */
	public function sign ($cert, $privateKey)
	{
		try {
			openssl_pkcs7_sign(
				$this->xmlFilePath,
				$this->tmpFilePath,
				"file://" . $cert,
				["file://" . $privateKey, ""],
				[],
				!PKCS7_DETACHED
			);
		} catch (Exception $e) {
			$sMensaje = "Afip Wsaa - Tra::sign() - No se pudo generar la forma PKCS#7 en el TRA.";
			throw new Exception($sMensaje, $e->getCode(), $e);
		}

		return $this->obtenerContenidoArchivoTmp();
	}
}