<?php 

namespace Afip\Ws\Tickets\Autenticacion;

use Exception;
use SimpleXMLElement;
use Afip\Ws\Config;

/**
 * Class Ta
 * @package Afip\Ws\Tickets\Autenticacion
 */
class Ta
{
	/**
	 * @var string
	 */
	protected $xmlFilePath;

	/**
	 * @param Config $config
	 */
	public function __construct(Config $config)
	{
		$this->config = $config;
		$this->xmlFilePath = $config->getStoragePath() . DIRECTORY_SEPARATOR . $config->alias . "_ta.xml";
	}

	/**
	 * @param $content
	 * @return bool
	 */
	public function crearArchivo($content)
	{
		$ta = @file_put_contents($this->xmlFilePath, $content);

		$created = ($ta === FALSE) ? false : true;

		return $created;
	}

	public function getRealPath()
	{
		return $this->xmlFilePath;
	}

	/**
	 * @return null|SimpleXMLElement
	 */
	private function parseXml()
	{
		$xml = null;
		$content = @file_get_contents($this->xmlFilePath);

		if (!empty($content)) {
			$xml = $this->xml = new SimpleXMLElement($content);
		}

		return $xml;
	}

	/**
	 * Valida el Ticket de Autorizacion
	 *
	 * @return bool
	 */
	public function isValid ()
	{
		$validated = false;
		$xml 	   = $this->parseXml();

		if (!empty($xml)) {
			$exp = (string) $xml->header->expirationTime;
			if ( strtotime($exp) - ( date('U') ) > 3600 ) {
				$validated = true;
			}
		}

		return $validated;
	}

	public function read ()
	{
		return $this->toArray();
	}

	/**
	 * Devuelve los datos necesarios en formato Array
	 *
	 * @return array
	 */
	public function toArray ()
	{
		//Validar Ticket de Autorizacion
		if ($this->isValid()) {
			$xml = $this->parseXml();

			return array(
				'token'       => (string) $xml->credentials->token,
				'sign'        => (string) $xml->credentials->sign,
				'expiration'  => (string) $xml->header->expirationTime,
				'uniqueid'    => (string) $xml->header->uniqueId
			);
		}

		return null;
	}
}