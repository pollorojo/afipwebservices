<?php 

namespace Afip\Ws;

use Exception;

class Config
{
	private $data;

	private $keys = [
		"alias",
		"storage",
		"cuit",
		"crt",
		"key"
	];

	const ENV_TESTING = 'testing';

	const ENV_PRODUCTION = 'production';

	protected static $envs = [
		self::ENV_TESTING,
		self::ENV_PRODUCTION
	];

	protected $env = self::ENV_TESTING;

	protected static $wsdlFiles = [
		Wsaa::WS_NAME => 'wsaa.wsdl',
		Wsfe::WS_NAME => 'wsfe_v1.wsdl'
	];

	protected static $dirFiles;

	/**
	 * @param array $arr
	 * @throws Exception
	 */
	public function __construct(array $arr, $env=null)
	{
		//$this->validate($arr);
		$this->validarParametros($arr);

		$this->data['alias']   = $arr['alias'];
		$this->data['storage'] = $arr['storage'];
		$this->data['crt']     = $arr['crt'];
		$this->data['cuit']    = $arr['cuit'];
		$this->data['key']     = $arr['key'];

		self::$dirFiles = dirname(__FILE__);

		if (!empty($env)) {
			$this->setEnv($env);
		}
	}

	/**
	 * @param array $arr
	 * @throws Exception
	 */
	public function validarParametros($arr)
	{
		//Validar campos necesarios
		$c = count(array_intersect($this->keys, array_keys($arr)));
		if ($c < count($this->keys)) {
			throw new Exception(__METHOD__.": La configuracion es incorrecta, faltan valores.", 1);
		}

		if (!is_dir($arr['storage'])) {
			throw new Exception(__METHOD__.": No se encontro el directorio ".$arr['storage'], 2);
		}

		if (!is_readable($arr['crt']) and !is_readable($arr['key'])) {
			throw new Exception(__METHOD__.": Los archivos .crt y .key deben ser editables", 3);
		}
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function __get ($key)
	{
		return (array_key_exists($key, $this->data)) ? $this->data[$key] : null;
	}

	/**
	 * @return string
	 */
	public function getStoragePath()
	{
		return rtrim($this->storage, '/') . DIRECTORY_SEPARATOR;
	}

	public function getTaPath ()
	{
		return $this->getStoragePath() . $this->alias . "_ta.xml";
	}

	public function getTraPath ()
	{
		return $this->getStoragePath() . $this->alias . "_tra.xml";
	}

	/**
	 * @param string $key
	 * @return string|null
	 */
	public static function getWsdl($key)
	{
		$filePath = null;

		if (array_key_exists($key, self::$wsdlFiles)) {
			$file = self::$dirFiles . DIRECTORY_SEPARATOR . self::$wsdlFiles[$key];
			if (file_exists($file)) {
				$filePath = $file;
			}
		}

		return $filePath;
	}

	public function isEnv($env)
	{
		return ($env === $this->env);
	}

	public function setEnv($env)
	{
		if (in_array($env, self::$envs)) {
			$this->env = $env;
		}
	}
}