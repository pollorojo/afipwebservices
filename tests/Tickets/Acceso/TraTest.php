<?php 

namespace Afip\Ws\Tests\Ticekts\Acceso;

use Afip\Ws\Tickets\Acceso\Tra;

class TraTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->file = __DIR__."/../../files/TRA.xml";
	}

	public static function tearDownAfterClass ()
	{
		if (is_file(__DIR__."/../../files/TRA.xml")) {
			unlink(__DIR__."/../../files/TRA.xml");
			@unlink(__DIR__."/../../files/TRA.tmp");
		}
	}

	public function xmlProvider ()
	{
		return array(
			array('<?xml version="1.0" encoding="UTF-8"?><testing>'.rand(1000, 9999).'</testing>')
		);
	}

	/**
	 * @dataProvider xmlProvider
	 */
	public function testCreateTicket ($xml)
	{
		$tra = new Tra($this->file);
		$tra->create(array());

		$this->assertTrue(is_file($this->file));
	}
}