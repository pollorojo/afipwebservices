<?php 

namespace Afip\Ws\Tests\Tickets\Autenticacion;

use Afip\Ws\Tickets\Autenticacion\Ta;

/**
* 
*/
class TaTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->file = __DIR__."/../../files/TA.xml";
	}

	public static function tearDownAfterClass ()
	{
		if (is_file(__DIR__."/../../files/TA.xml")) {
			unlink(__DIR__."/../../files/TA.xml");
		}
	}

	public function xmlProvider ()
	{
		return array(
			array('<?xml version="1.0" encoding="UTF-8"?><testing>'.rand(1000, 9999).'</testing>')
		);
	}

	/**
	 * @dataProvider xmlProvider
	 */
	public function testCreateTicket ($xml)
	{
		$ta = new Ta($this->file);
		$ta->create($xml);

		$this->assertTrue(is_file($this->file));
		$this->assertEquals($xml, file_get_contents($this->file));
	}

	/**
	 * @dataProvider xmlProvider
	 */
	public function testReadContent ($xml)
	{
		$ta = new Ta($this->file);
		$ta->create($xml);

//		$this->assertEquals(array('token' => "", 'sign' => "", 'expiration' => "", 'uniqueid' => ""), $ta->read());
	}
}