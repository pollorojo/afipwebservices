<?php 

namespace Afip\Ws\Tests;

use Mockery;
use stdClass;
use Afip\Ws\Wsn;
use Afip\Ws\Wsaa;
use Afip\Ws\Wsfe;
use Afip\Ws\Config;

/**
* 
*/
class WsnTest extends \PHPUnit_Framework_TestCase
{
	public function setUp ()
	{
		$this->cabecera = array(
			'CantReg'  => "",
			'CbteTipo' => "",
			'PtoVta'   => ""
		);

		$this->detalle = array(
			'Concepto'     => "",
			'DocTipo'      => "",
			'DocNro'       => "",
			'CbteDesde'    => "",
			'CbteHasta'    => "",
			'CbteFch'      => "",
			'FchServDesde' => "",
			'FchServHasta' => "",
			'FchVtoPago'   => "",
			'ImpTotal'     => "",
			'ImpTotConc'   => "",
			'ImpNeto'      => "",
			'ImpOpEx'      => "",
			'ImpIVA'       => "",
			'ImpTrib'      => "",
			'MonId'        => "",
			'MonCotiz'     => ""
		);

		$this->iva = array(
			array(
				'Id'      => "",
				'BaseImp' => "",
				'Importe' => ""
			),
			array(
				'Id'      => "",
				'BaseImp' => "",
				'Importe' => ""
			)
		);

		$this->caeRegInterface = $this->getMock('Afip\Ws\CAERequestInterface');
		
		$arr = array(
			"alias"     => "testing",
			"cuit" 		=> "30999999998",
			"storage"   => __DIR__."/files/",
			"servicios" => array(
				"wsaa" => array(
					"url"  => "https://wsaahomo.afip.gov.ar/ws/services/LoginCms",
					"wsdl" => __DIR__."/files/wsaa.wsdl"
				),
			),
			"crt"  => __DIR__."/files/mi_test.crt",
			"key"  => __DIR__."/files/mi_test.key"
		);

		$config = new Config($arr);
		$this->wsn = new Wsn($config);
	}

	/**
     * @expectedException     Exception
     * @expectedExceptionCode 1
     */
	public function testExceptionNoService ()
	{
		$wsn = new Wsn(new Config(array()));
		$wsn->make("aaa");
	}

	
	/**
     * @expectedException     Exception
     * @expectedExceptionCode 1
     */
	public function testExceptionBuildFormatCabecera ()
	{
		$this->caeRegInterface->expects($this->once())
						->method('setCaeCabReq')
						->will($this->returnValue(array()));

		$this->wsn->buildCAERequest(1, $this->caeRegInterface);
	}

	/**
     * @expectedException     Exception
     * @expectedExceptionCode 2
     */
	public function testExceptionBuildFormatDetalle ()
	{
		$this->caeRegInterface->expects($this->once())
						->method('setCaeCabReq')
						->will($this->returnValue($this->cabecera));

		$this->caeRegInterface->expects($this->once())
						->method('setCaeDetReq')
						->will($this->returnValue(array()));

		$this->wsn->buildCAERequest(1, $this->caeRegInterface);
	}

	/**
     * @expectedException     Exception
     * @expectedExceptionCode 3
     */
	public function testExceptionBuildFormatIva ()
	{
		$this->caeRegInterface->expects($this->once())
						->method('setCaeCabReq')
						->will($this->returnValue($this->cabecera));

		$this->caeRegInterface->expects($this->once())
						->method('setCaeDetReq')
						->will($this->returnValue($this->detalle));

		$this->caeRegInterface->expects($this->once())
						->method('setCaeDetReqIva')
						->will($this->returnValue(array(array())));

		$this->wsn->buildCAERequest(1, $this->caeRegInterface);
	}

	public function testBuildCAERequest ()
	{
		$this->caeRegInterface->expects($this->once())
						->method('setCaeCabReq')
						->will($this->returnValue($this->cabecera));

		$this->caeRegInterface->expects($this->once())
						->method('setCaeDetReq')
						->will($this->returnValue($this->detalle));

		$this->caeRegInterface->expects($this->once())
						->method('setCaeDetReqIva')
						->will($this->returnValue($this->iva));

		$req = $this->wsn->buildCAERequest(1, $this->caeRegInterface);

		$this->assertTrue(is_array($req));
		$this->assertArrayHasKey("FeCAEReq", $req);
	}
}