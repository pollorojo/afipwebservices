<?php 

namespace Afip\Ws\Tests;

use Mockery;
use stdClass;
use Afip\Ws\Wsaa;
use Afip\Ws\Config;
use Afip\Ws\Tickets\Autenticacion\Ta;
use Afip\Ws\Tickets\Acceso\TraXml;
use Afip\Ws\Tickets\Acceso\TraTmp;

/**
* 
*/
class WsaaTest extends \PHPUnit_Framework_TestCase
{
	public function setUp ()
	{
		$arr = array(
			"alias"     => "testing",
			"cuit" 		=> "30999999998",
			"storage"   => __DIR__."/files/",
			"servicios" => array(
				"wsaa" => array(
					"url"  => "https://wsaahomo.afip.gov.ar/ws/services/LoginCms",
					"wsdl" => __DIR__."/files/wsaa.wsdl"
				),
			),
			"crt"  => __DIR__."/files/mi_test.crt",
			"key"  => __DIR__."/files/mi_test.key"
		);

		$this->config = new Config($arr);
	}

	public function tearDown ()
	{
		Mockery::close();
	}

	public function testLoginCrearNuevoTicketAutenticacion ()
	{
		$ta = Mockery::mock('Afip\Ws\Tickets\Autenticacion\Ta', array($this->config->getTaPath()));

		$ta->shouldReceive('isValid')
			->once()
			->andReturn(false);
		$ta->shouldReceive('create')->once()->andReturn(true);
		$ta->shouldReceive('read')->once()->andReturn(array("token" => "", "sign"=>""));

		$tra = Mockery::mock('Afip\Ws\Tickets\Acceso\Tra', array($this->config->getTraPath()));
		$tra->shouldReceive('create')->once();
		$tra->shouldReceive('sign')->once();

		//SoapClient
		$client = $this->getMockFromWsdl($this->config->wsdl('wsaa'));

		$return = new stdClass;
		$return->loginCmsReturn = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
			<loginTicketResponse version="1">
			    <header>
			        <source>CN=wsaahomo, O=AFIP, C=AR, SERIALNUMBER=CUIT 00000000000</source>
			        <destination>C=ar, O=testing, SERIALNUMBER=CUIT 00000000000, CN=server</destination>
			        <uniqueId>999999500</uniqueId>
			        <generationTime>2015-11-25T00:46:01.114-03:00</generationTime>
			        <expirationTime>2015-11-25T12:46:01.114-03:00</expirationTime>
			    </header>
			    <credentials>
			        <token></token>
			        <sign></sign>
			    </credentials>
			</loginTicketResponse>';

		$client->expects($this->once())->method('loginCms')->will($this->returnValue($return));

		$wsaa = new Wsaa("wsfe", $this->config, $client, $ta, $tra);
		$auth = $wsaa->login();

		$this->assertTrue(is_array($auth));
	}

	public function filesProvider ()
	{
		return array(
			array("test", __DIR__."/files/mi_ws.wsdl", __DIR__."/files/mi_test.crt", __DIR__."/files/mi_test.key")
		);
	}

	public function ticketsProvider ()
	{
		return array(
			array(__DIR__."/files/TA.xml", __DIR__."/files/TRA.xml")
		);
	}
}