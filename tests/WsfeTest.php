<?php 

namespace Afip\Ws\Tests;

use Mockery;
use stdClass;
use Exception;
use SoapFault;
use Afip\Ws\Wsfe;

class WsfeTest extends \PHPUnit_Framework_TestCase
{
	public function setUp ()
	{
		$this->client = $client = $this->getMockFromWsdl(__DIR__."/files/wsfe_v1.wsdl");

		$auth = array(
			"Token" => "",
			"Sign"  => "",
			"Cuit"  => ""
		);

		$this->wsfe = new Wsfe($auth, $client);
	}

	/**
     * @expectedException     Exception
     * @expectedExceptionCode 1
     */
	public function testSoapFault ()
	{
		$soapFault = new SoapFault("5", "Error generado en Test");

		$this->client->expects($this->once())
					->method('FECAESolicitar')
					->will($this->returnValue($soapFault));
		
		$this->wsfe->FECAESolicitar(array("FeCAEReq"=>array()));
	}

	public function testObtenerErrores()
	{
		$Err       = new stdClass;
		$Err->Code = 500;
		$Err->Msg  = "No se corresponden token y firma. Usuario no autorizado a realizar esta operación";

		$Result = new stdClass;
		$Result = (object) array(
			"RegXReq" => (object) array(),
			"Errors"  => (object) array(
				"Err" => $Err
			)
		);

		$return = new stdClass;
		$return->FECAESolicitarResult = $Result;

		//WS
		$this->client->expects($this->once())
					->method('FECAESolicitar')
					->will($this->returnValue($return));
		
		//Action
		$this->wsfe->FECAESolicitar(array("FeCAEReq"=>array()));

		$errors = $this->wsfe->getErrors();
		$this->assertTrue(is_array($errors));
		$this->assertEquals(500, $errors[0]->Code);
	}
	
	public function testFECAESolicitar()
	{
		$return = new stdClass;
		$return->FECAESolicitarResult = 'Test!';

		//WS
		$this->client->expects($this->once())
					->method('FECAESolicitar')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FECAESolicitar(array("FeCAEReq"=>array()));
		
		$this->assertEquals("Test!", $result);
	}

	public function testFECAESolicitarAndResponseInterface()
	{
		$return = new stdClass;
		$return->FECAESolicitarResult = (object) array(
			"FeCabResp" => "Testing",
			"FeDetResp" => (object) array(
				"FECAEDetResponse" => (object) array(
					"CAE" => 1
				)
			)
		);

		$caeRespInterface = $this->getMock('Afip\Ws\CAEResponseInterface');
		$caeRespInterface->expects($this->once())
						->method('getCaeResponse');

		//WS
		$this->client->expects($this->once())
					->method('FECAESolicitar')
					->will($this->returnValue($return));
		
		//Action
		$feCaeReq = array(
			"FeCAEReq" => array(
				'FeCabReq' => "",
				'FeDetReq' => array(
					'FECAEDetRequest' => ""
				)
			)
		);
		$result = $this->wsfe->FECAESolicitar($feCaeReq, $caeRespInterface);

		$this->assertTrue(is_object($result));
		$this->assertEquals("Testing", $result->FeCabResp);
	}

	public function testFECompConsultar()
	{
		$results = new stdClass;
		$results->ResultGet = (object) array("Concepto"=>1);

		$return = new stdClass;
		$return->FECompConsultarResult = $results;

		//WS
		$this->client->expects($this->once())
					->method('FECompConsultar')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FECompConsultar(6, 1, 1);
		
		$this->assertEquals(1, $result->Concepto);
	}

	public function testFECompTotXRequest()
	{
		$xReg = new stdClass;
		$xReg->RegXReq = 5;

		$return = new stdClass;
		$return->FECompTotXRequestResult = $xReg;

		//WS
		$this->client->expects($this->once())
					->method('FECompTotXRequest')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FECompTotXRequest();
		
		$this->assertEquals(5, $result);
	}

	public function testFEParamGetTiposDoc()
	{
		$ResultGet = new stdClass;
		$ResultGet->ResultGet = (object) array(
			"DocTipo" => (object) array(
				'Id'       => 1,
				'Desc'     => "Descripcion",
				'FchDesde' => "",
				'FchHasta' => ""
			)
		);

		$return = new stdClass;
		$return->FEParamGetTiposDocResult = $ResultGet;

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposDoc')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposDoc();

		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFECompUltimoAutorizado()
	{
		$return = new stdClass;
		$return->FECompUltimoAutorizadoResult = (object) array(
			"PtoVta"   => 1,
			"CbteTipo" => 6,
			"CbteNro"  => 123
		);

		//WS
		$this->client->expects($this->once())
					->method('FECompUltimoAutorizado')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FECompUltimoAutorizado(1, 6);
		
		$this->assertTrue(is_object($result));
		$this->assertEquals(123, $result->CbteNro);
	}

	public function testFEParamGetTiposCbte()
	{
		$return = new stdClass;
		$return->FEParamGetTiposCbteResult = (object) array(
			"ResultGet" => (object) array(
				"CbteTipo" => array(
					array('Id' => 1, 'Desc' => "Descripcion",'FchDesde' => "", 'FchHasta' => ""),
					array('Id' => 2, 'Desc' => "Descripcion",'FchDesde' => "", 'FchHasta' => "")
				)
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposCbte')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposCbte();
		$this->assertEquals(2, $result[1]["Id"]);
	}

	public function testFEParamGetPtosVenta()
	{
		$return = new stdClass;
		$return->FEParamGetPtosVentaResult = (object) array(
			"ResultGet" => (object) array(
				"PtoVta" => array(
					array('Nro' => 1, 'EmisionTipo' => "Demo",'Bloqueado' => "Demo", 'FchBaja' => ""),
					array('Nro' => 2, 'EmisionTipo' => "Demo",'Bloqueado' => "Demo", 'FchBaja' => "")
				)
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetPtosVenta')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetPtosVenta();
		
		$this->assertTrue(is_array($result));
		$this->assertEquals(2, $result[1]["Nro"]);
	}

	public function testFEParamGetTiposConcepto()
	{
		$ConceptoTipo = new stdClass;
		$ConceptoTipo->Id       = 1;
		$ConceptoTipo->Desc     = "Demo";
		$ConceptoTipo->FchDesde = "";
		$ConceptoTipo->Fchbaja  = "";

		$return = new stdClass;
		$return->FEParamGetTiposConceptoResult = (object) array(
			"ResultGet" => (object) array(
				"ConceptoTipo" => $ConceptoTipo
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposConcepto')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposConcepto();
		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFEParamGetTiposOpcional()
	{
		$OpcionalTipo = new stdClass;
		$OpcionalTipo->Id       = 1;
		$OpcionalTipo->Desc     = "Demo";
		$OpcionalTipo->FchDesde = "";
		$OpcionalTipo->Fchbaja  = "";

		$return = new stdClass;
		$return->FEParamGetTiposOpcionalResult = (object) array(
			"ResultGet" => (object) array(
				"OpcionalTipo" => $OpcionalTipo
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposOpcional')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposOpcional();
		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFEParamGetTiposIva()
	{
		$IvaTipo = new stdClass;
		$IvaTipo->Id       = 1;
		$IvaTipo->Desc     = "Demo";
		$IvaTipo->FchDesde = "";
		$IvaTipo->Fchbaja  = "";

		$return = new stdClass;
		$return->FEParamGetTiposIvaResult = (object) array(
			"ResultGet" => (object) array(
				"IvaTipo" => $IvaTipo
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposIva')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposIva();
		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFEParamGetTiposTributos()
	{
		$TributoTipo = new stdClass;
		$TributoTipo->Id       = 1;
		$TributoTipo->Desc     = "Demo";
		$TributoTipo->FchDesde = "";
		$TributoTipo->Fchbaja  = "";

		$return = new stdClass;
		$return->FEParamGetTiposTributosResult = (object) array(
			"ResultGet" => (object) array(
				"TributoTipo" => $TributoTipo
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposTributos')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposTributos();
		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFEParamGetTiposMonedas()
	{
		$Moneda = new stdClass;
		$Moneda->Id       = 1;
		$Moneda->Desc     = "Demo";
		$Moneda->FchDesde = "";
		$Moneda->Fchbaja  = "";

		$return = new stdClass;
		$return->FEParamGetTiposMonedasResult = (object) array(
			"ResultGet" => (object) array(
				"Moneda" => $Moneda
			)
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetTiposMonedas')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetTiposMonedas();
		$this->assertTrue(is_array($result));
		$this->assertEquals(1, $result[0]["Id"]);
	}

	public function testFEParamGetCotizacion ()
	{
		$ResultGet = new stdClass;
		$ResultGet->MonId    = "ar";
		$ResultGet->MonCotiz = 2.50;
		$ResultGet->FchCotiz = "";

		$return = new stdClass;
		$return->FEParamGetCotizacionResult = (object) array(
			"ResultGet" => $ResultGet
		);

		//WS
		$this->client->expects($this->once())
					->method('FEParamGetCotizacion')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEParamGetCotizacion("ar");
		
		$this->assertTrue(is_object($result));
		$this->assertEquals(2.50, $result->MonCotiz);
	}

	public function testFEDummy ()
	{
		$Result = new stdClass;
		$Result->AppServer  = "demo";
		$Result->DbServer   = "dbDemo";
		$Result->AuthServer = "demo";

		$return = new stdClass;
		$return->FEDummyResult = $Result;

		//WS
		$this->client->expects($this->once())
					->method('FEDummy')
					->will($this->returnValue($return));
		
		//Action
		$result = $this->wsfe->FEDummy();
		
		$this->assertTrue(is_object($result));
		$this->assertEquals("dbDemo", $result->DbServer);
	}
	
}